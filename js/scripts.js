

// Разбираемся с ресайзом и отслеживанием девайсов

var MOBILEMAXWIDTH = 1080;
(function() {

	var oldWindowWidth = null;

	$(window).on("resize", function() {
		trackResolution();
	});

	function trackResolution() {
		var w = $(window).width();
		if(w <= MOBILEMAXWIDTH && (oldWindowWidth > MOBILEMAXWIDTH || !oldWindowWidth)) {
			$(document).trigger("to-mobile");
			console.log("to-mobile");
		} else if(w > MOBILEMAXWIDTH && (oldWindowWidth <= MOBILEMAXWIDTH || !oldWindowWidth)) {
			$(document).trigger("to-desktop");
			console.log("to-desktop");
		}
		oldWindowWidth = w;
	}

	function detectMobile() {
		var agent = navigator.userAgent.toLowerCase();
		if(agent.match(/ios|ipad|iphone|android|bb|playbook|iemobile|windows phone/)) {
			$("html").addClass("mobile");
		} else {
			$("html").addClass("desktop");
		}
	}

	detectMobile();
	trackResolution();

})();



// после сборки

$(document).on("ready", function() {

	// Лендинг-баннер на главной странице
	if($(".site-header[data-animate='true']").length > 0 && !$("html").hasClass("mobile") && $(window).width() > MOBILEMAXWIDTH) {
		initLandingHeading();
	}

	// тень под шапкой

		// следим за скроллом и показываем тень под шапкой
		$(window).on("scroll", function() {
			var head = $(".site-header");
			if($(window).scrollTop() > 50) {
				head.addClass("shadow");
			} else if($(".site-header .neq4-projects").attr("data-active") != "true") {
				head.removeClass("shadow");
			}
		});
		// показываем тень когда выезжает шапка тоже
		$(document).on("active-true", ".site-header .neq4-projects", function() {
			$(".site-header").addClass("shadow");
		});
		// убираем тень когда шапка скрыта и скролл в нуле
		$(document).on("active-false", ".site-header .neq4-projects", function() {
			if($(window).scrollTop() < 50) {
				$(".site-header").removeClass("shadow");
			}
		});


	// клон emerge (показывание элементов с анимацией)
	initEmergeFake();

	// прокрутка до элемента с айди из хеша ссылки
	setTimeout(function () {
		$("body, html").scrollTop(0);
	},10);
	//initAnchorScroll();

	// При показе меню, показываем его и сдвигаем боди
	$(".site-menu").on("active-true", function() {
		$("body").addClass("show-menu");
		if($(".toggle-neq4-projects").attr("data-active") == "true") {
			$(".toggle-neq4-projects").click();
		}
	});
	$(".site-menu").on("active-false", function() {
		$("body").removeClass("show-menu");
		if($(".toggle-neq4-projects").attr("data-active") == "true") {
			$(".toggle-neq4-projects").click();
		}
	});

	// при показе доп.проектов увеличиваем хеадер
	$(".neq4-projects").on("active-true", function() {
		$("body").addClass("show-projects");
	});
	$(".neq4-projects").on("active-false", function() {
		$("body").removeClass("show-projects");
	});

	// табы
	initTabs();

});


$(window).on("load", function() {
	// прокрутка до элемента с айди из хеша ссылки
	setTimeout(initAnchorScroll, 500);
});

// простые переключалки
$(document).on("click", "a[data-toggle]", function() {
	var target = $($(this).attr("data-toggle"));
	var	toggleStatus = (target.attr("data-active") == "false" || !target.attr("data-active") || target.attr("data-active") == "") ? true : false;
	target.attr("data-active", toggleStatus);
	target.trigger("active-"+toggleStatus);
	var selector = $(this).attr("data-toggle");
	// console.log($("*[data-toggle='"+selector+"']"));
	$("*[data-toggle='"+selector+"']").attr("data-active", ($(this).attr("data-active") == "true" ? false : true));
	//$(this).attr("data-active", $(this).attr("data-active") == "true" ? false : true);
});


// табы
function initTabs() {
	$(document).on("click", "a.tab-link", function(e) {
		var link = $(this);
		var links = link.siblings(".tab-link");
		var target = $(link.attr("href"));
		// hide all tabs
		var tabs = [];
		for(var i = 0; i < links.length; i++) {
			var el = $(links.eq(i).attr("href"));
			if(el.hasClass("active")) {
				el.trigger("active-false").removeClass("active");
			}
		}
		// deselect links
		links.removeClass("active");
		// select tab and link
		link.addClass("active");
		target.addClass("active");
		target.trigger("active-true");
		// prevent
		e.preventDefault();
	});
}


// подделка emerge.js (показывание элементов с анимацией)
function initEmergeFake() {

    // Запускаем анимации
    $(".emergefake").each(function(index) {
    	var el = $(this);
    	var delay = el.attr("data-emergefake-delay") ? parseInt(el.attr("data-emergefake-delay")) : 0;
    	var order = index;
    	setTimeout(function() {
    		el.addClass("emergefake-show");
    	}, (order)*(delay > 0 ? delay : 150))
    });
}

// прокрутка до элемента с айди из хеша ссылки
var anchorScrollEventsInited = false;
function initAnchorScroll() {
	if(location.hash) {
		var target = $(location.hash);
		if(target.length > 0) {
			$("html, body").animate({
				scrollTop: target.offset().top - 120
			}, 600);
		}
	}

	if(anchorScrollEventsInited) {
		return;
	}

	$(document).on("click", "a[href*='#']:not(.tab-link)", function(event) {
		var hash = this.href.split("#")[1];
		var target = $("#" + hash);
		if(hash && target.length > 0) {
			setTimeout(function() {
				$("html, body").animate({
					scrollTop: target.offset().top - 120
				}, 600);
			});
		}

		if($(location)[0].href.split("#")[0] == this.href.split("#")[0]) {
			event.preventDefault();
		}
	});

	anchorScrollEventsInited = true;
}

// // Лендинг-баннер на главной странице
// // http://tympanus.net/Development/ArticleIntroEffects/index.html
// function initLandingHeading() {

// 	$("html").addClass("with-landing");

// 	// detect if IE : from http://stackoverflow.com/a/16657946
// 	var ie = (function(){
// 		var undef,rv = -1; // Return value assumes failure.
// 		var ua = window.navigator.userAgent;
// 		var msie = ua.indexOf('MSIE ');
// 		var trident = ua.indexOf('Trident/');

// 		if (msie > 0) {
// 			// IE 10 or older => return version number
// 			rv = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
// 		} else if (trident > 0) {
// 			// IE 11 (or newer) => return version number
// 			var rvNum = ua.indexOf('rv:');
// 			rv = parseInt(ua.substring(rvNum + 3, ua.indexOf('.', rvNum)), 10);
// 		}

// 		return ((rv > -1) ? rv : undef);
// 	}());


// 	// disable/enable scroll (mousewheel and keys) from http://stackoverflow.com/a/4770179
// 	// left: 37, up: 38, right: 39, down: 40,
// 	// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
// 	var keys = [32, 37, 38, 39, 40], wheelIter = 0;

// 	function preventDefault(e) {
// 		e = e || window.event;
// 		if (e.preventDefault)
// 		e.preventDefault();
// 		e.returnValue = false;
// 	}

// 	function keydown(e) {
// 		for (var i = keys.length; i--;) {
// 			if (e.keyCode === keys[i]) {
// 				preventDefault(e);
// 				return;
// 			}
// 		}
// 	}

// 	function touchmove(e) {
// 		preventDefault(e);
// 	}

// 	function wheel(e) {
// 		// for IE
// 		//if( ie ) {
// 			//preventDefault(e);
// 		//}
// 	}

// 	function disable_scroll() {
// 		window.onmousewheel = document.onmousewheel = wheel;
// 		document.onkeydown = keydown;
// 		document.body.ontouchmove = touchmove;
// 	}

// 	function enable_scroll() {
// 		window.onmousewheel = document.onmousewheel = document.onkeydown = document.body.ontouchmove = null;
// 	}

// 	var docElem = window.document.documentElement,
// 		scrollVal,
// 		isRevealed,
// 		noscroll,
// 		isAnimating,
// 		container = $("html").get(0);
// 		//trigger = container.querySelector( 'button.trigger' );
// 	var time;

// 	function scrollY() {
// 		return window.pageYOffset || docElem.scrollTop;
// 	}

// 	function scrollPage() {
// 		scrollVal = scrollY();

// 		if( noscroll && !ie ) {
// 			if( scrollVal < 0 ) return false;
// 			// keep it that way
// 			window.scrollTo( 0, 0 );
// 		}

// 		if( $(container).hasClass('notrans') ) {
// 			$(container).removeClass('notrans' );
// 			return false;
// 		}

// 		if( isAnimating ) {
// 			return false;
// 		}

// 		if( scrollVal <= 0 && isRevealed ) {
// 			toggle(0);
// 		}
// 		else if( scrollVal > 0 && !isRevealed ){
// 			toggle(1);
// 		}
// 	}

// 	function toggle( reveal ) {
// 		isAnimating = true;

// 		if( reveal ) {
// 			$(container).addClass('landing-hide' );
// 			clearTimeout(time);
// 			time = setTimeout(function() {
// 				$(container).addClass('landing-stop-animation');
// 			},1000);
// 			// $(container).addClass('landing-hide' );
// 		}
// 		else {
// 			noscroll = true;
// 			disable_scroll();
// 			$(container).removeClass('landing-hide' );
// 			$(container).removeClass('landing-stop-animation');
// 			if($(".site-menu").attr("data-active") == "true") {
// 				$(".toggle-site-menu").click();
// 			}
// 			if($(".neq4-projects").attr("data-active") == "true") {
// 				$(".toggle-neq4-projects").click();
// 			}
// 		}

// 		// simulating the end of the transition:
// 		setTimeout( function() {
// 			isRevealed = !isRevealed;
// 			isAnimating = false;
// 			if( reveal ) {
// 				noscroll = false;
// 				enable_scroll();
// 			}
// 		}, 1200 );
// 	}

// 	// refreshing the page...
// 	var pageScroll = scrollY();
// 	noscroll = pageScroll === 0;

// 	disable_scroll();

// 	if( pageScroll ) {
// 		isRevealed = true;
// 		$(container).addClass('notrans' );
// 		$(container).addClass('landing-hide' );
// 		clearTimeout(time);
// 		time = setTimeout(function() {
// 			$(container).addClass('landing-stop-animation');
// 		},1000);
// 	}

// 	window.addEventListener( 'scroll', scrollPage );
// 	//trigger.addEventListener( 'click', function() { toggle( 'reveal' ); } );

// }
